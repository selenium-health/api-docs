Getting Set Up
------------

1. Convert documentation
```shell
# install APIs documentation formats converter
npm install [-g] widdershins
# run this to convert Swagger *.json file into Slate *.md file format
widdershins --language_tabs 'shell:Shell' 'ruby:Ruby' --summary swagger.json -o source/index.html.md
```

2. Run documentation server
```shell
# run this to run locally
bundle install
bundle exec middleman server
```

3. Build docs for the deploy
```shell
bundle exec middleman build --clean
```

4. Deploy
```shell
cap staging deploy
```

Or, you can use the included rake tasks:
```shell
rake docs:build    # Build docs for the deploy (convert all needed files into the static one)
rake docs:convert  # Use 'Widdershins' to converts swagger.json file into markdown suitable for use by a renderer, such as Slate
rake docs:deploy   # Deploying already builded (static files) documentation into the remote server
rake docs:run      # Run documentation server locally
```

__Slate__ Beautiful static documentation for your API.
You can find more information here: https://github.com/slatedocs/slate

__widdershins__ OpenAPI / Swagger, AsyncAPI & Semoasa definitions to Slate / Shins compatible markdown 
https://github.com/Mermade/widdershins
set :ssh_options, {
    keys: '~/.ssh/id_rsa',
    forward_agent: true,
    auth_methods: %w(publickey)
}

server 'api.aura.social',
       user: 'deployer',
       roles: %w{web app}